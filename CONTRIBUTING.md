## Developer Certificate of Origin + License

By contributing to GitLab B.V., You accept and agree to the following terms and
conditions for Your present and future Contributions submitted to GitLab B.V.
Except for the license granted herein to GitLab B.V. and recipients of software
distributed by GitLab B.V., You reserve all right, title, and interest in and to
Your Contributions. All Contributions are subject to the following DCO + License
terms.

[DCO + License](https://gitlab.com/gitlab-org/dco/blob/master/README.md)

_This notice should stay as the first item in the CONTRIBUTING.md file._


## How to contribute

It is important to understand that the whole idea of Conversational Development
is a working draft. You can participate in many different ways:

- You can propose or suggest any change by
  [creating a merge request](https://gitlab.com/gitlab-com/conversationaldevelopment-com/merge_requests)
- [Create an issue](https://gitlab.com/gitlab-com/conversationaldevelopment-com/issues/new)
  if you have any questions or have found an issue.
- If you want to be a case study, please fill and submit
  [Case Study Participant form](https://goo.gl/forms/jDgMnaiFomLirlo23)
- Host a meetup to discuss Conversational Development
- Help to spread information about Conversational Development by sharing it on
  social networks
